all: baldir-build next-build audit-report

changes=git diff-index --quiet HEAD
changes_detected=0

TMP_BALDIR_FR_DIR=/tmp/tmp-baldir.fr
BALDIR_FR_DEPLOY_DIR=baldir.fr_gridsome/dist/
TMP_NEXT_BALDIR_FR_DIR=/tmp/tmp-next-baldir.fr
NEXT_BALDIR_FR_DEPLOY_DIR=next.baldir.fr/dist/
TMP_AUDITS_BALDIR_FR_DIR=/tmp/tmp-audits-baldir.fr
AUDITS_BALDIR_FR_DEPLOY_DIR=audits.baldir.fr/public/

fetch:
	git fetch

detect-changes: fetch
	$(shell ${changes})
ifneq (0,$?)
	@echo changes detected
else
	@echo no changes detected
endif

checkout: detect-changes
	$(shell ${changes})
ifneq (0,$?)
	git reset --hard origin/master
endif

next-build:
	$(MAKE) -C next.baldir.fr build-11ty;

next-deploy:
	rm -rf ${TMP_NEXT_BALDIR_FR_DIR}
	mkdir -p ${TMP_NEXT_BALDIR_FR_DIR}
	curl --location --output ${TMP_NEXT_BALDIR_FR_DIR}/artifacts.zip "${NEXT_BALDIR_FR_ARTIFACT_URL}"
	unzip	-o ${TMP_NEXT_BALDIR_FR_DIR}/artifacts.zip -d ${TMP_NEXT_BALDIR_FR_DIR}/unzipped
	rm -rf ${NEXT_BALDIR_FR_DEPLOY_DIR}
	mkdir -p ${NEXT_BALDIR_FR_DEPLOY_DIR}
	cp -Rf ${TMP_NEXT_BALDIR_FR_DIR}/unzipped/next.baldir.fr/dist/* ${NEXT_BALDIR_FR_DEPLOY_DIR}
	rm -rf ${TMP_NEXT_BALDIR_FR_DIR}

baldir-deploy:
	rm -rf ${TMP_BALDIR_FR_DIR}
	mkdir -p ${TMP_BALDIR_FR_DIR}
	curl --location --output ${TMP_BALDIR_FR_DIR}/artifacts.zip "${BALDIR_FR_ARTIFACT_URL}"
	unzip	-o ${TMP_BALDIR_FR_DIR}/artifacts.zip -d ${TMP_BALDIR_FR_DIR}/unzipped
	rm -rf ${BALDIR_FR_DEPLOY_DIR}
	mkdir -p ${BALDIR_FR_DEPLOY_DIR}
	cp -Rf ${TMP_BALDIR_FR_DIR}/unzipped/baldir.fr_gridsome/dist/* ${BALDIR_FR_DEPLOY_DIR}
	rm -rf ${TMP_BALDIR_FR_DIR}

audit-clean:
	rm -rf audits.baldir.fr/public

audit-report: audit-clean
	$(MAKE) -C audits.baldir.fr build

audit-deploy:
	rm -rf ${TMP_AUDITS_BALDIR_FR_DIR}
	mkdir -p ${TMP_AUDITS_BALDIR_FR_DIR}
	curl --location --output ${TMP_AUDITS_BALDIR_FR_DIR}/artifacts.zip "${AUDITS_BALDIR_FR_ARTIFACT_URL}"
	unzip	-o ${TMP_AUDITS_BALDIR_FR_DIR}/artifacts.zip -d ${TMP_AUDITS_BALDIR_FR_DIR}/unzipped
	rm -rf ${AUDITS_BALDIR_FR_DEPLOY_DIR}
	mkdir -p ${AUDITS_BALDIR_FR_DEPLOY_DIR}
	cp -Rf ${TMP_AUDITS_BALDIR_FR_DIR}/unzipped/audits.baldir.fr/public/* ${AUDITS_BALDIR_FR_DEPLOY_DIR}
	rm -rf ${TMP_AUDITS_BALDIR_FR_DIR}

baldir-ci:
	${MAKE} -C baldir.fr_gridsome npm-ci
baldir-build:
	${MAKE} -C baldir.fr_gridsome build
baldir-dev:
	${MAKE} -C baldir.fr_gridsome dev
baldir-clean:
	${MAKE} -C baldir.fr_gridsome clean
