# Baldir

## Static sites


<!-- tree -d -L 1 -->

```
.
├── 11ty                   #
├── _11ty                  #
├── audits.baldir.fr       #
├── backlog                #
├── content                #
├── definitions.baldir.fr  #
├── dist                   #
├── infra                  #
├── lab                    #
├── lighthouse             #
├── next.baldir.fr         # New site (in construction) uses pages from `content` folder. Built with 11ty
├── src                    #
├── static                 #
└── uploads                #

```


## Build sites

## Host sites

## Next steps

- Extract current site in its own subfolder
  - Keep content outside the subfolder
- Generate a rich version and a light version (for low end mobile or slow connexion, no js required)

Before

```
.
├── 11ty
├── _11ty
├── audits.baldir.fr
├── backlog
├── content
├── definitions.baldir.fr
├── dist
├── infra
├── lab
├── lighthouse
├── next.baldir.fr
├── src
├── static
└── uploads

```

After

```
.
├── 11ty
├── _11ty
├── audits.baldir.fr
├── backlog
├── baldir.fr_gridsome
├── baldir.fr_11ty
├── content
├── definitions.baldir.fr
├── infra
├── lab
├── lighthouse
├── next.baldir.fr
├── static
└── uploads

```
