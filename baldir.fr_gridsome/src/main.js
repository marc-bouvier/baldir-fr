// Import main css
import '~/assets/style/index.scss'

// Import default layout so we don't need to import it to every page
import DefaultLayout from '~/layouts/Default.vue'

// The Client API can be used here. Learn more: gridsome.org/docs/client-api
export default function (Vue, { router, head, isClient }) {
  head.link.push({
    rel: 'authorization_endpoint',
    href: 'https://auth.baldir.fr'
  })

  head.link.push({
    rel: 'webmention',
    href: 'https://webmention.io/baldir.fr/webmention'
  })
  head.link.push({
    rel: 'pingback',
    href: 'https://webmention.io/baldir.fr/xmlrpc'
  })
  head.link.push({
    rel: 'me',
    href: 'https://twitter.com/marcbouvier_'
  })
  head.link.push({
    rel: 'me',
    href: 'https://github.com/marc-bouvier'
  })
  head.link.push({
    rel: 'me',
    href: 'https://framagit.org/marc-bouvier'
  })
  head.link.push({
    rel: 'me',
    href: 'https://www.linkedin.com/in/profileofmarcbouvier/'
  })
  head.link.push({
    rel: 'me',
    href: 'mailto:m.bouvier.dev@gmail.com'
  })
  head.link.push({
    rel: 'me',
    href: 'https://stackoverflow.com/users/444769/marc-bouvier'
  })
  // Add public GPG key
  head.link.push({
    rel: 'pgpkey',
    href: '/me/marc-bouvier.gpg.pub'
  })

  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
}
