---
layout: post
lang: fr
title: En 2022, focus de Framasoft sur l'usage du numérique dans les associations
date: 2022-01-19T23:00:00Z
description: En ce début d'année 2022, l'association a publié un article pour expliquer
  les objectifs de cette année. Et pour 2022, framasoft veut porter un focus tout
  particulier à l'usage du numérique dans les associations. Le logo est de JosephK
  sous licence Creative Commons By-SA 4.0
cover_image: "/2022-01-20/biglogo-notxt.png"
tags:
- Communs
- Logiciel-Libre
- Open-Source
- Associatif

---
[Framasoft](https://framasoft.org) est une association d’éducation populaire, un groupe d’ami·es convaincu·es qu’un monde numérique émancipateur est possible, persuadé·es qu’il adviendra grâce à des actions concrètes sur le terrain et en ligne avec vous et pour vous !

En ce début d'année 2022, [l'association a publié un article](https://framablog.org/2021/12/08/ce-que-framasoft-pourrait-faire-en-2022-grace-a-vos-dons/) pour expliquer les objectifs de cette année.

Et pour 2022, framasoft veut porter un focus tout particulier à l'usage du numérique dans les associations.

TLDR;

- Émancip’Asso pour une cohérence numérique

  - but : proposer aux associations des acteurs en mesure de les accompagner dans leur démarche de « dégafamisation ».

  - offrir un programme de formation aux acteurs qui proposent aux associations des services et accompagnements spécifiques

- Un observatoire sur les pratiques numériques libres

  - enquêtes, analyses

  - mieux comprendre les outils, les habitudes, les expériences des personnes qui utilisent le logiciel libre. Ou mieux comprendre pourquoi elles ne l’utilisent pas !

- Nextcloud (suite collaborative libre)

  - travailler à le rendre plus simple et plus accessible pour toutes et tous

  - en améliorant le logiciel, sa notoriété ou sa documentation

  - proposer une offre spécialement destinée aux associations

Et aussi

- peertube v5 annoncé pour 2022

  - résoudre certains points de frustration et améliorer l’utilisabilité,

  - donner plus de maîtrise aux gestionnaires d’instances comme aux vidéastes

  - proposer un outil permettant d’éditer (de façon basique) des vidéos

  - améliorer les capacités de synchronisation entre YouTube et PeerTube

    - GUI

- mobilizon v4 annoncé pour 2022

  - outil d’importation depuis Meetup et Facebook

  - vue carte

  - outil de recherche global multi-instances

- Framanifeste

  - achever sa rédaction

  - Il définira aussi le cadre de nos actions et les valeurs qui fondent notre identité

- CHATONS

  - volonté de "dé-framasoftiser" la coordination du collectif

  - lancer une réflexion sur les objectifs

- UPLOAD : Université Populaire Libre, Ouverte, Autonome et Décentralisée

  - Partager savoirs et connaissances

- Livres en commun (anciennement Framabook)

  -  se concentrer sur la création et la production de communs culturels.

  - s'opposer au modèle actuel de monde de l'édition. Reconnaître la réalité du travail de l'auteur•ice en amont, en le rémunérant directement par une bourse d’écriture versée aux auteur⋅ices pour reconnaître plus équitablement leur travail de création.

- Soutiens à la culture, au libre et aux communs

- Organisation de rencontres

Plus de détails dans l'article complet :

https://framablog.org/2021/12/08/ce-que-framasoft-pourrait-faire-en-2022-grace-a-vos-dons/
