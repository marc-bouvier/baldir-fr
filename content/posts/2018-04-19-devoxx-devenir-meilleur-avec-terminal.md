---
layout: post
title: "Devoxx - Devenir meilleur avec le terminal (french)"
description: Devoxx France 2018. Gagner des super pouvoirs avec le terminal (S. Ehret)
tags: 
- Script 
- French 
- Terminal 
- Bash 
- Linux 
- Linux French 
date: 2018-04-19
---
[Gagner des super pouvoirs avec le terminal (S. Ehret)](https://www.youtube.com/watch?v=mxRpBHar_BQ)

* `ranger` : explorateur de fichiers
* ̀ feh` : visualisateur d'images
* plusieurs types de shell existent : bash, zsh, fish (performant mais pas compatiblr bash)
* `xclip` : copier un texte dans le presse papier 
