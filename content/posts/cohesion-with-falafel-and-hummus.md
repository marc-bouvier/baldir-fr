---
layout: post
lang: en
title: "Cohesion with falafel and hummus"
date: 2022-06-23T14:15:00Z
description: "Today I wanted to cook something. What about some falafel? I dug a bit more and found a can of hummus. It made me think about cohesion."
tags:
- Cohesion
cover_image: "/cohesion-falafel-and-hummus/fallafel-hoummous-cohesive.jpg"
---

Today I wanted to cook something. What about some falafel? I dug a bit more and found a can of hummus. It made me think about cohesion.

![To illustrate low cohesion. A picture of packs of food. A box of falafel preparation, some other boxes, and then a can of hummus.](/cohesion-falafel-and-hummus/fallafel-hoummous-low-cohesion.jpg)

I told myself: “Those 2 items goes well together. Maybe they should be close together”.

![To illustrate high cohesion. A picture of packs of food rearranged. The box of falafel preparation is next to the can of hummus.](/cohesion-falafel-and-hummus/fallafel-hoummous-cohesive.jpg)


This is what we call “Cohesion” in software development.

When I see falafel, I don't forget I also get seasoning with it. I don't have to look for it or remember I bought some hummus. It's there!

If I see only one of them, I can lose motivation. “I need to go to the store and buy falafel” or ”I need to make my own hummus”. Sometimes I want to do it, sometimes I don't. Having them close together allows me to make quick and low energy decision.

In a codebase, the more a concept is close to another, the more we see it and options come to our mind.

In other hand if both concepts exist far away, we may rewrite the second one because we did'nt know it existed elsewhere.



