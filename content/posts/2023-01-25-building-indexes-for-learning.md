---
title: "Building indexes for learning"
date: 2023-01-25
lang: "en"
description: "As a generalist developer, I cannot dive deeply in each topic I encounter. But I still want to have it accessible somewhere. Building indexes for learning allows me to “store” look for it when I need it."
tags:
- KnowledgeBase
- GeneralistDeveloper
---

Open the following like and search "building index for learning".
https://techleadjournal.dev/episodes/63/

An index card is
- A name;
- A summary with **my own words**;
- A link ton the thing to be indexed
- (optional) some additional infos (in my case: Tags)

As a generalist developer, I cannot dive deeply in each topic I encounter. But I still want to have it accessible somewhere. Building indexes for learning allows me to "store" look for it when I need it.

[In another post](2023-01-5-automate-building-index-for-learning.md) I show you how I semi-automated it with Obsidian.




