---
layout: post
title: "Devoxx DDD et RGPD (french)"
description: Les keynotes de Devoxx France 2018 et conférence sur DDD, Event Sourcing et RGPD
tags: 
- Devoxx 
- RGPD 
- Domain-Driven-Design 
- French
date: 2018-04-19
---
Les keynotes de Devoxx France 2018 et conférence sur DDD, Event Sourcing et RGPD.

- [DDD & Event Sourcing à la rescousse pour implémenter la RGPD ? (G. Lours, J. Avoustin)](https://www.youtube.com/watch?v=RK4mrpro2B4)
- [La e residence estonienne et l’entrepreneuriat sans frontieres (A. Castaignet)](https://www.youtube.com/watch?v=qTpKVBZoMs4)
- [French Road (E. Pesenti)](https://www.youtube.com/watch?v=0gv9jWD1EN8)