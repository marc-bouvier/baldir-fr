---
date: 2018-03-09
title: "Event Sourcing with Kafka, RabbitMQ and JPA"
description: In this article, the author give us some way to implement Event-Sourcing with Kafka and RabbitMQ.
tags: 
- Event-Sourcing 
- Kafka RabbitMQ 
- JPA 
- How-To
---
In this article, the author give us some way to implement Event-Sourcing with Kafka and RabbitMQ. http://engineering.pivotal.io/post/event-source-kafka-rabbit-jpa/
