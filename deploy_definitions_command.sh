#!/bin/bash
this_command_path=$(realpath $0)
this_folder=$(dirname $this_command_path)
cd $this_folder
# No need to do anything but checkout
# because it is pure HTML at the right location
make checkout
