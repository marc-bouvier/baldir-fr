---
layout: post
title: "ADR Architecture Decision Records (french)"
description: "Les ADR sont une approche pour documenter les décisions d'architecture logicielle afin d'en conserver le pourquoi et les alternatives considérées."
date: 2018-04-20
lang: fr

tags:
- Architecture
- Enterprise-Architecture
- Methodology
- French
- How-To
- Architecture-Decision-Records

---
Les ADR sont une approche pour documenter les décisions d'architecture logicielle afin d'en conserver le pourquoi et les alternatives considérées.

* quelques templates : https://github.com/marc-bouvier/architecture_decision_record
