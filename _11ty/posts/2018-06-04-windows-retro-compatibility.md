---
layout: post
title: "Windows relationship with retro compatibility"
description: "Some history and topics about Microsoft Windows compatibility strategies."
date: 2018-06-04

tags:
- Business-Model
- Api
- Architecture
---
* [How Microsoft Lost the API War](https://www.joelonsoftware.com/2004/06/13/how-microsoft-lost-the-api-war/)
* [From Win32 to Cocoa: A Windows user’s would-be conversion to Mac OS X](https://arstechnica.com/gadgets/2018/05/what-microsoft-could-learn-from-apple/)
