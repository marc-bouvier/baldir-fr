---
layout: post
title: "Feature toggle with Togglz"
description: "Switch features without redeployment with feature toggles."

tags:
- Feature-Toggle
- Jsf
- Java
---
Switch features without redeployment with feature toggles.
[Feature toggle with Togglz](https://www.togglz.org/quickstart.html)
