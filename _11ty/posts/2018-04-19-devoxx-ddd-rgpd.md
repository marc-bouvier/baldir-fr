---
layout: post
title: "Devoxx DDD et RGPD (french)"
date: 2018-04-19
description: "Les keynotes de Devoxx France 2018 et conférence sur DDD, Event Sourcing et RGPD"
lang: fr

tags:
- Devoxx
- RGPD
- Domain-Driven-Design
- French
---
Les keynotes de Devoxx France 2018 et conférence sur DDD, Event Sourcing et RGPD.

- [DDD & Event Sourcing à la rescousse pour implémenter la RGPD ? (G. Lours, J. Avoustin)](https://www.youtube.com/watch?v=RK4mrpro2B4)
- [La e residence estonienne et l’entrepreneuriat sans frontieres (A. Castaignet)](https://www.youtube.com/watch?v=qTpKVBZoMs4)
- [French Road (E. Pesenti)](https://www.youtube.com/watch?v=0gv9jWD1EN8)
