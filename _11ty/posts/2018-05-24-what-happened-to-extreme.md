---
layout: post
title: "What ever happened to being eXtreme?"
date: 2018-05-24
description: "Rachel Davies - What Ever Happened to Being eXtreme?"

tags:
- Extreme-Programming
- Agility
- NewCrafts
- Video
---
Rachel Davies - What Ever Happened to Being eXtreme?

Back in the day, I was in that first flurry of developers who got excited about eXtreme Programming (XP). The idea of writing automated tests and pair programming seemed radical at the time. After years of being stuck in waterfall hell, we got to speak to real users and deliver code - it was a wonderful time to be a developer! Fifteen years later, "Agile" somehow stopped being about software development and agile meetups are full of project managers and business analysts. Second-generation software developers roll their eyes when they hear anyone talking about "agile" and have become detached from a movement that seems to be about dragging them into even more meetings and away from their beloved code.

Yet these same developers are faced with a plethora of devices that their code needs to work in and cloud services that give them the ability to scale at speed. Techniques from XP are becoming more relevant to organizations that want to pivot fast and shift to Continuous Delivery. The tag line for Extreme Programming Explained book was "embrace change". Let's revisit old-school XP and find out what it has to offer that's been forgotten by the mainstream.

Come to this talk to hear practical experience from teams using XP for 11 years, what we dropped and what elements of XP we've adapted now we have better infrastructure for global development.
<iframe src="https://player.vimeo.com/video/22102https://player.vimeo.com/video/2210248464846" width="640" height="360" frameborder="0" allowfullscreen></iframe>
