---
layout: post
title: "GOTO 2018 • Modular Monoliths • Simon Brown"
description: "GOTO 2018 • Modular Monoliths • Simon Brown"
date: 2018-12-05
tags:
- Abstraction
- Architecture
- Encapsulation
- Monolith
- MicroService
- Discipline
- Software-Design
- Enterprise-Architecture
- Object-Oriented-Programming
- Packaging
---
GOTO 2018 • Modular Monoliths • Simon Brown
<iframe width="560" height="315" src="https://www.youtube.com/embed/5OjqD-ow8GE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[GOTO 2018 • Modular Monoliths • Simon Brown](https://www.youtube.com/watch?v=5OjqD-ow8GE)
