---
layout: post
title: "Add audience interactions to your presentations"
date: 2018-04-23
description: "Nowadays, it is possible to add interactivity to your presentations : polls, quizzes, questions. We are going to see a few tools that can allow such a thing."

tags:
- Feedback
- Soft-Skills
- Training Tooling
- Educational
---
Nowadays, it is possible to add interactivity to your presentations : polls, quizzes, questions. We are going to see a few tools that can allow such a thing.

menti.com : users type a pin code and can join a room created with mentimeter.com

sli.do : allows to switch between questions and poll in the same session.

kahoot.it : fun way to add interactivity to a presentation. Administation on kahoot.com
