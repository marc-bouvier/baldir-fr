---
layout: "post"

title: "\U0001F3A6 Turn any keyboard into a Stream Deck on Windows"

lang: en

date: 2021-12-07T13:54:00Z

description: "With a key binder and an extra keyboard or numpad we can create a budget stream deck to command OBS Studio or any application that can accept key bindings."

cover_image: "/2021-12-07/tony_cuozzo_number_pad_2011_cc-by-2-0_flickr_toncu_5623834240_400x386.jpg"

tags:

- DIY
- Windows
- How-To
- OBS
- Content-Streaming

---
With a key binder and an extra keyboard or numpad we can create a budget stream deck to command OBS Studio or any application that can accept key bindings.

## Guides

[HID macros](http://hidmacros.eu/) is a tool that can bind keys to commands on Windows. It can differentiate between keyboards so there won't be conflicts between multiple keyboards.

* [Create a Streamdeck for LESS THAN 5$ - YouTube](https://www.youtube.com/watch?v=IxWHswdB3Lw)
* [Turn ANY Keyboard Into A Stream Deck! - YouTube](https://www.youtube.com/watch?v=jdI_IeP1K3o)

## Num pads

* [Nedis Numeric Keypad - Pavé numérique NEDIS sur LDLC](https://www.ldlc.com/fiche/PB00270709.html)
* [Pavé numérique USB 19 touches compatible Windows et Mac - Pavé numérique Générique sur LDLC](https://www.ldlc.com/fiche/PB00199063.html)

## Credits

* Tony Cuozzo - Number Pad - 2011
  * CC BY 2.0
  * https://www.flickr.com/photos/toncu/5623834240/
  * Resized to 400x386px
  * Renamed : Tony_Cuozzo_Number_Pad_2011_CC-BY-2.0_flickr_toncu_5623834240_400x386.jpg
