---
date: 2018-07-23
title: "Career advice for programmers by Trisha Gee"
layout: post

tags:
- Career
- Innovation
- Technology
---
GOTO 2013 • Career Advice for Programmers • Trisha Gee.

<iframe width="560" height="315" src="https://www.youtube.com/embed/LlAn452X4Lc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
[GOTO 2013 • Career Advice for Programmers • Trisha Gee](https://www.youtube.com/watch?v=LlAn452X4Lc)
