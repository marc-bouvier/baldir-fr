---
layout: post
title: "Event sourcing expliqué par un projet simple (french)"
date: 2018-05-01
description: "Event sourcing par l’exemple (Loïc Knuchel)"
lang: fr

tags:
- French
- Event-Sourcing
---
Event sourcing par l’exemple (Loïc Knuchel)

<iframe width="560" height="315" src="https://www.youtube.com/embed/l0c4oR4JPr4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> Présentation plutôt bien faite et simple de l'event sourcing en Français (BreizhCamp 2018).
