---
layout: post
title: "Unit test you architecture with ArchUnit"
description: "Enforce automatically some architecture concerns with ArchUnit"

tags:
- Testing
- Architecture
---
Enforce automatically some architecture concerns with ArchUnit
[https://www.archunit.org/](https://www.archunit.org/)
