---
layout: post
title: "Retour sur les panama papers et les bases de donnés graphe (Benoit Simard) (french)"
date: 2018-05-03
description: "Au printemps 2016, le premier article de presse sur les 'Panama papers' a été publié. Il s'agit de la plus grande 'fuites' de l'histoire avec plus de 3T de données brutes."
lang: fr

tags:
- French
- Graph
- Neo4J
- NoSql
- Fraud-Detection
---
Au printemps 2016, le premier article de presse sur les "Panama papers" a été publié. Il s'agit de la plus grande 'fuites' de l'histoire avec plus de 3T de données brutes.
Cette conférence vous détaillera les technologies que l'ICIJ (International Consortium Of Investigate Journalists) a utilisé pour fournir aux journalistes une interface simple pour leur recherche.

<iframe width="560" height="315" src="https://www.youtube.com/embed/6aSXzYXi_HE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
