---
layout: post
title: "On n'est pas chez Google ici!"
date: 2018-05-02
description: "Devoxx France 2018. On n’est pas chez Google ici ! (B. Bujon)"
lang: fr

tags:
- Conduct-Of-Change
- Tooling
- Organizational-Culture
- Productivity
- Soft-Skills
- French
---
On n’est pas chez Google ici ! (B. Bujon)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LeONtn2ECxo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
