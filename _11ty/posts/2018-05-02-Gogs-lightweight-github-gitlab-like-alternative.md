---
layout: post
title: "Gogs : a lightweight standalone alternative to GitHub and GitLab"
date: 2018-05-02
description: "Gogs: a lightweight alternative to Github and GiLab"

tags:
- Git
- Tooling
- Open-Source
- Platform
---
Gogs: a lightweight alternative to Github and GiLab

https://try.gogs.io/
