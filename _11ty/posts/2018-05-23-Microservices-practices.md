---
layout: post
title: "Best Practices for Building a Microservice Architecture"
date: 2018-05-23
description: "Best Practices for Building a Microservice Architecture"

tags:
- MicroService
- How-To
- Guidelines
---

[Best Practices for Building a Microservice Architecture](https://www.vinaysahni.com/best-practices-for-building-a-microservice-architecture) by Vinay Sahni
