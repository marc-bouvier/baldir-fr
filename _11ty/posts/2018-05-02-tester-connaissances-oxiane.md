---
layout: post
title: "Testez vos connaissances avec Oxiane (french)"
date: 2018-05-02
description: "Quizzes techniques par Oxiane pour connaitre son niveau dans diverses technologies"
lang: fr

tags:
- Training
- French
- Branding
- Java
- Docker
- Kotlin
- Javascript
- React
- Angular
- Html5
- DevOps
- Scrum
- Kanban
- Bdd
- Android

---
Quizzs Oxiane

http://www.oxiane.com/testez-vos-connaissances/
