---
layout: post
title: "Convert dom NodeList to Array of nodes"
date: 2018-05-18
description: "How to convert a Dom NodeList to an Array of nodes."

tags:
- Javascript
- DOM
- How-To
---
How to convert a Dom NodeList to an Array of nodes.

`var nodesArray = [].slice.call(document.querySelectorAll("div"));`

This tip was found [here](https://davidwalsh.name/nodelist-array)
