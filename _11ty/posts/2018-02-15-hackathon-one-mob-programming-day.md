---
layout: post
title: "Mob programming (French)"
description: "Une journée en Mob programming"
date: 2018-02-15
lang: fr

tags:

- Mob-Programming
- Agility
- French

---
Une journée en Mob programming.

TLDR; Faire travailler toute une équipe sur une seule tâche

- Un seul ordinateur
- Une zone de travail assez grande où tout le monde doit être bien installé
- un projecteur ou écran facilement visible par tous et avec une bonne résolution
- des gens (dev, product owners...)
- une tâche
- style Driver/Navigators

Mob programming qu'est-ce que c'est? Un présentation très claire et complète par
ici : [http://blog.soat.fr/2014/05/le-mob-programming-presentation/](http://blog.soat.fr/2014/05/le-mob-programming-presentation/)
