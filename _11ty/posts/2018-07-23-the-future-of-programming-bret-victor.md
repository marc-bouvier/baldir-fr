---
layout: post
date: 2018-07-23
title: "The Future of programming - Bret Victor"
description: "Bret Victor - The Future of Programming"

tags:
- Career
- Innovation
- Technology
---
[Bret Victor - The Future of Programming](https://www.youtube.com/watch?v=8pTEmbeENF4)

<iframe width="560" height="315" src="https://www.youtube.com/embed/8pTEmbeENF4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

