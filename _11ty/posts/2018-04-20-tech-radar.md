---
layout: post
title: "Technology radar (french)"
description: "Cartographie les technologies que ton organisation utilise et leur niveau de maturité."
date: 2018-04-20
lang: fr

tags:
- Architecture
- Enterprise-Architecture
- Methodology
- French
- Architecture-Decision-Records

---

https://www.thoughtworks.com/radar

Fais ton propre radar: https://www.thoughtworks.com/radar/byor

Utilité

* cartographier les technologies
* tech owner
* mesurer le niveau de maturité de l'entreprise

Complémentaire avec les ADR (Architecture Decision Records)
