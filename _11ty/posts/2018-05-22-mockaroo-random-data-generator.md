---
layout: post
title: "Mockaroo - a realistic random data generator"
date: 2018-05-22
description: "Need some mock data to test your app? Mockaroo lets you generate up to 1,000 rows of realistic test data in CSV, JSON, SQL, and Excel formats."

tags:
- Testing
- Test-Fixture
---
Need some mock data to test your app? Mockaroo lets you generate up to 1,000 rows of realistic test data in CSV, JSON, SQL, and Excel formats.

[Mockaroo - realistic random data generator](https://mockaroo.com)
