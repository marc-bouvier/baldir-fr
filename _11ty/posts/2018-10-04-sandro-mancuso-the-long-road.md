---
layout: post
title: "Sandro Mancuso - The long road"
description: "DevTernity 2017 - Sandro Mancuso - The Long Road"

tags:
- Career
- Discipline
- Inspiration
---
Choosing the next career step in such a diverse and fast-paced industry is not an easy task. But when it comes to our careers, there is no right or wrong. Or there is? How do we know? In this talk we will be talking about different career choices, how can we choose good companies to work for, what we can learn from interviews and selection processes, when should we look for a new job, and how to change our working environment.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vQDnW265XKU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

[🚀 DevTernity 2017: Sandro Mancuso - The Long Road](https://www.youtube.com/watch?time_continue=1107&v=vQDnW265XKU)
