---
layout: post
title: "Tudor Girba : Software environmentalism"
date: 2018-07-28
description: "Tudor Girba : Software environmentalism"
tags:
- Debugging
- Pharo
- Moose
- Object-Oriented-Programming
- Organizational-Culture
- Technical-Dept
- Technology-Choice
- Tooling
- Smalltalk
---
We produce software systems at an ever increasing rate, but our ability to get cleanup after older systems does not keep up with that pace. An IDC study showed that there are some 10k mainframe systems in use containing some 200B LOC. This shows that software is not that soft, and that once let loose systems produce long lasting consequences. Because of the impact of our industry, we need to look at software development as a problem of environmental proportions. We must build our systems with recycling in mind. As builders of the future world, we have to take this responsibility seriously.

<iframe width="560" height="315" src="https://www.youtube.com/embed/KDHrtYGbUQ8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

[Tudor Girba - Software environmentalism](https://www.youtube.com/watch?v=KDHrtYGbUQ8)


* [@girba](https://twitter.com/girba)
* [@feenk](https://twitter.com/feenkcom)
* [gtoolkit](http://gtoolkit.org/)
* [@moosetechnology](https://twitter.com/moosetechnology)
* [Moose technology](http://moosetechnology.org/) Moose is a platform for software and data analysis.
  * It helps programmers craft custom analyses cheaply.
  * It's based on Pharo and it's open source under BSD/MIT.
* [@humaneA](https://twitter.com/humaneA)
* [Humane assesment](http://humane-assessment.com/)
* [@pharoproject](https://twitter.com/pharoproject)
* [The pharo project](https://pharo.org/)
