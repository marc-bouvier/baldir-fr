---
title: "Aurelien Barreau - Ecologie et nouveau pacte avec le vivant"
description: "Ecologie et nouveau pacte avec le vivant : Aurelien Barrau"
layout: post
lang: fr

tags :
- Ecology
- Inspiration
- French
---
Climax festival 2018

<iframe width="560" height="315" src="https://www.youtube.com/embed/H4wjc4FHpNY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

* [Ecologie et nouveau pacte avec le vivant : Aurelien Barrau](https://www.youtube.com/watch?v=H4wjc4FHpNY&feature=youtu.be)
