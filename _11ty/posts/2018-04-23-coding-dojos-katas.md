---
layout: post
title: "Coding dojos & Katas"
description: "Adoptez la pratique délibérée pour améliorer votre boîte à outils de Craft."
date: 2018-04-23
lang: fr

tags:
- Discipline
- Methodology
- Training
- French
- Software-Craftsmanship
---
Adoptez la pratique délibérée pour améliorer votre boîte à outils de Craft.

Faire des coding dojo de 1h30 : 1h kata, 30min review.
* http://codingdojo.org

Requis :
* 1 Pc avec de quoi coder dans le langage qu'on veut
* s'imposer une ou plusieurs contraintes: TDD, utiliser telle technique ...
* Proposer des Kata en pair programming entre 12h et 14h
