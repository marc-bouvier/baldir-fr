---
layout: post
title: "Event sourcing and CQRS - 9 things that make your journey painful (french)"
date: 2018-05-02
description: "9 things that will make your Event Sourcing/CQRS journey painful (Clément HELIOU)"
lang: fr

tags:
- French
- Event-Sourcing
- CQRS
- Domain-Driven-Design
---
9 things that will make your Event Sourcing/CQRS journey painful (Clément HELIOU)

<iframe width="560" height="315" src="https://www.youtube.com/embed/v1RdackDm60" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> [Projet en exemple](https://github.com/clementheliou/conference-management-system)
