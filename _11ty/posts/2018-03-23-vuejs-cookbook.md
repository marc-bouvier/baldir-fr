---
layout: post
title: "VueJs now provides a cookbook"
date: 2018-03-23
description: "VueJS now provides a cookbook to show some ways to build a application."

tags:
- VueJs
- Guidelines
- Testing
- Javascript
---
VueJS now provides [a cookbook][cookbook] to show some ways to build a application.

[cookbook]:https://vuejs.org/v2/cookbook/unit-testing-vue-components.html
