# Todo

## - [ ] Migrate from Gridsome to eleventy

Why? I want to have fast generated and lightweight accessible content.

Based on https://github.com/11ty/eleventy-base-blog
- [X] Allow french or english content `<html lang="en">` or `<html lang="fr">` depending on lang property (default to english)
- [ ] Podcast feed experiment
- [ ] Update content for accessibility
- [ ] check if posts/images correctly maps to /img
- [ ] CI : node 12
- [x] Adopt a license
- [ ] License logo and mention on rendered views
- [ ] Cleanup readme.md
- [ ] static content /me
- [ ] Cheat sheets layout
- [X] Fix tags
- [ ] Sort tags
- [ ] Fix broken resources
- [ ] Fix rendu bizarre : manque layout: post
  - http://localhost:8080/posts/2019-03-31-software-crafters-strasbourg-meetup/
  - http://localhost:8080/posts/2018-02-15-hackathon-one-mob-programming-day/
- [ ] Page : carte des meetups de Strasbourg
- [ ] Feed links
- [ ] Licence mention
- [ ] rel="me"
- [ ] 2018-08-13-hacking-your-work-life-balance-Jennifer-Wadella
  - add images
