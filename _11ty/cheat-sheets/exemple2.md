---
title: "Exemple de cheat sheet sans image"
date: 2020-09-05
description: "But : montrer un exemple de structure de cheat sheet"
---
But : montrer un exemple de structure de cheat sheet

- item 1
- item 2
- item 3

## Ressources

- [un lien](https://example.com)

## Changelog 

- [x] tag how-to
- [x] tag cheat sheet