Try not to run the build twice (in CI and then on the hosting)

- Produce artifacts in the CI build
- Download them from the hosting
- Simplify the deployment makefile
  - clean
  - copy from pre-built artifacts
