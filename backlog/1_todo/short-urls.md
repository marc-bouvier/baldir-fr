provide shortened links for the readers to share any article.

- add short-url attribute in meta data of content.
  - ex. shortlink-slug: my-link-20
  - use alternate semantic web for these links
  - "share short link" : https://u.baldir.fr/my-link-20
- one letter content code types
  - see http://tantek.pbworks.com/w/page/21743973/Whistle#whyshortURLs
    - http://tantek.pbworks.com/w/page/21743973/Whistle#design
