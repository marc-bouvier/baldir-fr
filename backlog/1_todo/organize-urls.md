Refactor website and sub-domains to make urls forever.


https://indieweb.org/permalink

> You should maintain permalinks to your posts so links to them keep working. This is the intent of "Cool URIs [sic] don't change"[1].
>

https://indieweb.org/URL_design

https://manas.tungare.name/blog/url-design-sins-16-things-that-dont-belong-in-urls




## Url structure

```
# Main site

baldir.fr/                  # current presentation of myself. What do I do. What is my impact on the world. What I did (portfolio).  https://manas.tungare.name/
baldir.fr/about             # https://snarfed.org/about
baldir.fr/software/         # projects and side projects (and maitenance status).e.g. https://manas.tungare.name/software/
baldir.fr/blog/             # blog. https://manas.tungare.name/blog/. blog post URL example: https://manas.tungare.name/blog/geotagging-photos-from-an-slr-using-a-second-cameras-gps-logger. related posts
baldir.fr/blog/slug-with-blog-post-contextual-info #
baldir.fr/blog/page/8       # blog pagination
baldir.fr/cheat-sheet/      # cheat sheets (quick references). Today they are mainly found in https://world.hey.com/marc.bouvier
baldir.fr/resume/           # resume. Generated from jsonResume. https://manas.tungare.name/resume
baldir.fr/publications/pdf/publication-name.pdf  # scientific publications
baldir.fr/consulting/       # What I can do as a consultant. e.g. https://manas.tungare.name/consulting
baldir.fr/static/           # Static assets. e.g. https://manas.tungare.name/static/blog/2012/08/Hawaii-Geotagged.png
# Url shortener

u.baldir.fr/                #

```
