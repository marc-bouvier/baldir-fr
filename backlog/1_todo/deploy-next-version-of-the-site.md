don't wait it is finished before updating the site.
deploy the "next" version in a subdomain so that both can live side by side.

- baldir.fr : current
- next.baldir.fr : next (in progress)

## Why?

I can try out the benefits and quirks of trunk based.
I can show an measure progress.
- Such as in a blog post.
- Such as accessibility / performance online measures

I can be relieved of stress of big bang release an focus on tiny visible increments.
I will need to learn how to split and prioritize tasks so that they are atomic and complete.
This is a current weakness of me.
And a part of the NoEstimate philosophy I also want to try out.

## Increments

- [next-version-mvp-01](backlog/1_todo/next-version-mvp-01.mdrsion-mvp-01.md)
- [next-version-mvp-02](backlog/1_todo/next-version-mvp-02.mdrsion-mvp-02.md)
- [next-version-mvp-03](backlog/1_todo/next-version-mvp-03.mdrsion-mvp-03.md)
- [next-version-mvp-04](backlog/1_todo/next-version-mvp-04.mdrsion-mvp-04.md)
