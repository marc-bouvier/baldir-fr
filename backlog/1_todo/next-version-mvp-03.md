
## Increment 2b

Produce minimal relevant content to the frontpage.
Provide accessibility measurements reports.

- be accessible
- frugal
- HTML5
- measure (manually if necessary) and make a report
  - accessibility level checklist (Frago)
    - https://disic.github.io/frago/
  - green it checklist
  - opquast checklist

## Increment 3

Add next relevant content to start professional website activity.

- Frontpage missing content
  - service offer presentation
- legal mentions
- CGV
- privacy policy
