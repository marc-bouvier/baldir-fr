## First increment

Prioritize and split the minimal pre-requisites (MVP) for a first deployable increment.

- What is it I want to release first?
  - [X] Front page (minimal). Hello World.
- What activities are needed?
  - [X] Setup DNS subdomain entry: `next.baldir.fr`
  - [X] Setup Nginx virtual host to expose `path_to_next_dist`
  - [X] Setup ssl certificate with letsencrypt
  - [X] Create a single `path_to_next_dist/index.html`
  - [X] Modify CI/CD pipeline to add the minimum to deploy the new dist
