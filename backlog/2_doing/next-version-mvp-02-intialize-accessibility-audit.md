## Increment 2

Initialize and automate Accessibility audit reporting.

- [X] setup arborescence
- [X] setup placeholder dataset
- [X] adapt dataset to actual websites
- [ ] host in thhps://audits.baldir.fr/
- [ ] run actual tests TODO move it into next task
  - Chrome
    - https://chrome.google.com/webstore/detail/usability-hike-find-usabi/ndlpokknholbnmpamepplidpkfnmplgf
    - https://chrome.google.com/webstore/detail/axe-devtools-web-accessib/lhdoppojpmngadmnindnejefpokejbdd
    - Lighthouse Developer tools
    - https://chrome.google.com/webstore/detail/headingsmap/flbjommegcjonpdmenkdiocclhjacmbi
    - https://chrome.google.com/webstore/detail/wave-evaluation-tool/jbbplnpkjmmeebjpijfedlgcdilocofh
  - VoiceOver
    - https://disic.github.io/guide-lecteurs_ecran/voiceover.html
- [ ] init pipeline
- [ ] produce artifact
  - [ ] Makefile
    - [ ] Run `audit/Makefile` from main makefile
- [ ] script to download artifact and deploy it from hosting
- Blog article
  - Translate previous one in FR
  - Write this one in FR

# ADR

https://adr.github.io/


# Article : Mise en place l'audit RGAA avec Frago

[Discussion avec Bertrand Keller](https://github.com/DISIC/frago/discussions/38)

## Arborescence Frago

Détail du contenu du dossier `./audit/`

```
.
├── config.toml
├── content
│   ├── _index.md // page d’accueil du site :: ⚠️ Ajouter `type: projects` dans l’entête du fichier en cas de mono projet => va afficher directement la page de synthèse de tous les audits
│   ├── audits // Les pages pour afficher les audits (accessibilité, qualité, performance…) pour chaque projet
│   │    ├── baldir-fr // Doit reprendre le nom du répertoire donné au projet dans `static`
│   │    │     └── index.md // ⚠️  pour avoir lister les pages sur l’accueil et avoir la page intermédiaire qui liste tous les rapports
│   │    └── next_baldir-fr // Doit reprendre le nom du répertoire donné au projet dans `static`
│   │          └── index.md

```

Arborescence générale du dépôt:

```
.
├── audit             # contenu de frago (voir arborescence décrite plus haut)
├── baldir.fr         # projet/générateur du site baldir.fr
├── infra             # scripts d'hébergement
├── next.baldir.fr    # projet/générateur du site next.baldir.fr
├── .gitlab-ci.yml    # pipeline de CI/CD gitlab
├── Makefile          # scripts d'automatisation
└── README.md
```

Pré-requis:
- go installé
- hugo installé

Créer un dossier `audit`

Créer un fichier `config.toml`

```toml
baseURL = "https://audit.baldir.fr"
title = "Baldir" # Ce qui s'affiche sur le site.

[module]
[[module.imports]]
  path = "github.com/disic/frago"

# Utilisés dans les documents légaux
[params]
  organisation = "Baldir"
  [params.contacts]
    email = "m.bouvier.dev@gmail.com"
    address = "22 B RUE D'ADELSHOFFEN, 67300, SCHILTIGHEIM, FRANCE"
```

Initialiser le projet hugo.

```shell
hugo mod init gitlab.com/marc-bouvier/baldir-fr/audit
# WARN 2022/04/19 23:58:32 module "github.com/disic/frago" not found; either add it as a Hugo Module or store it in "/Users/marco/sources/baldir/baldir-fr/audit/themes".: module does not exist
# go: creating new go.mod: module gitlab.com/marc-bouvier/baldir-fr/audit
# go: to add module requirements and sums:
#         go mod tidy
```

Télécharger le thème Frago sous forme de Go module.

```shell
hugo mod get -u
# go: no module dependencies to download
# go: added github.com/disic/frago v1.7.1
# hugo: collected modules in 500 ms
```

Tester la génération.

```shell
hugo serve
#Start building sites …
#hugo v0.97.3+extended darwin/arm64 BuildDate=unknown
#WARN 2022/04/20 00:12:07 .Path when the page is backed by a file is deprecated and will be removed in a future release. We plan to use Path for a canonical source path and you probably want to check the source is a file. To get the current behaviour, you can use a construct similar to the one below:
#
#  {{ $path := "" }}
#  {{ with .File }}
#        {{ $path = .Path }}
#  {{ else }}
#        {{ $path = .Path }}
#  {{ end }}
#
#
#Re-run Hugo with the flag --panicOnWarning to get a better error message.
#WARN 2022/04/20 00:12:08 baldir-fr/accessibility/context.yml is not well filed, maybe the last value for date last audit is missing
#WARN 2022/04/20 00:12:08 next-baldir-fr/accessibility/context.yml is not well filed, maybe the last value for date last audit is missing
#
#                   | EN
#-------------------+-----
#  Pages            | 40
#  Paginator pages  |  0
#  Non-page files   |  5
#  Static files     | 43
#  Processed images |  0
#  Aliases          |  6
#  Sitemaps         |  1
#  Cleaned          |  0
#
#Built in 520 ms
#Watching for changes in /Users/marco/sources/baldir/baldir-fr/audit/content/{audits}
#Watching for config changes in /Users/marco/sources/baldir/baldir-fr/audit/config.toml, /Users/marco/sources/baldir/baldir-fr/audit/go.mod
#Environment: "development"
#Serving pages from memory
#Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
#Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
#Press Ctrl+C to stop

```

Commande pour construire le site.

```shell
HUGO_ENV="production" hugo --buildFuture --gc --minify --cleanDestinationDir©
```

Enveloppe Makefile pour encapsuler les taches automatiques

```makefile
all: build

serve:
	hugo serve

build:
	HUGO_ENV="production" hugo --buildFuture --gc --minify --cleanDestinationDir

```

