module.exports = {
  ci: {
    collect: {
      url: [
        'https://next.baldir.fr/',
        // 'https://baldir.fr/cheat-sheets/',
        // 'https://baldir.fr/tags/',
        // 'https://baldir.fr/tag/VueJs/',
        // 'https://baldir.fr/posts/tech-digest-26th-march-2018/',
        // 'https://baldir.fr/posts/write-your-own-tdd-lib-in-code-wars-and-codingame/',
        // 'https://baldir.fr/posts/simple-chat-with-vue-js-and-rocket-chat/'
      ],
      numberOfRuns: 1, // Set low to speed up the test runs. Default is 3.
      headful: false, // Show the browser which is helpful when checking the config
      settings: {
        disableStorageReset: true, // Don't clear localStorage / IndexedDB / etc before loading the page
        preset: 'desktop',
        chromeFlags: '--headless --disable-gpu --no-sandbox --disable-dev-shm-usage --no-first-run'
      }
    },
    upload: {
      // https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/getting-started.md#configure-lighthouse-ci
      // target: 'lhci',
      target: 'filesystem',
      // target: 'temporary-public-storage',
      // serverBaseUrl: 'http://lhserver:9001', // url of the server when run with docker compose
      // token: "de1efa21-1612-491e-a720-47f01d45fa2d" // build token generated when the command `lhci wizard` was run
    },
    assert:{
      // https://github.com/GoogleChrome/lighthouse-ci/blob/main/docs/getting-started.md#add-assertions
      preset: 'lighthouse:recommended',
    },
    server:{},
    wizard:{}
  }
};
