// This is where project configuration and plugin options are located. 
// Learn more: https://gridsome.org/docs/config

// Changes here requires a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
    siteName: 'Marc Bouvier',
    siteDescription: 'Mon portfolio',
    
    templates: {
        Post: '/posts/:title',
        Tag: '/tag/:id'
      },

    plugins: [
        {
            // Create posts from markdown files
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'Post',
                path: 'content/posts/*.md',
                refs: {
                    // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
                    tags: {
                        typeName: 'Tag',
                        create: true
                    }
                }
            }
        },
        {
            // Create cheat-sheets from markdown files
            use: '@gridsome/source-filesystem',
            options: {
                typeName: 'CheatSheet',
                path: 'content/cheat-sheets/*.md',
                refs: {
                    // Creates a GraphQL collection from 'tags' in front-matter and adds a reference.
                    tags: {
                        typeName: 'Tag',
                        create: true
                    },
                }
            }
        },
        {
            use: "gridsome-source-rss",
            options: {
              feedUrl: "https://www.ronjeffries.com/feed.xml",
              typeName: 'RssRonJeffries',
              // Parser options, see: https://www.npmjs.com/package/rss-parser
              // parser: new Parser()
            }
          },
    ],
    transformers: {
        //Add markdown support to all file-system sources
        remark: {
            externalLinksTarget: '_blank',
            externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
            anchorClassName: 'icon icon-link',
            plugins: [
                '@gridsome/remark-prismjs'
            ]
        }
    },
}