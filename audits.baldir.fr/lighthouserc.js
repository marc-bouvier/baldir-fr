module.exports = {
  ci: {
    collect: {
      url: [
        // 'https://baldir.fr',
        'https://next.baldir.fr',
        'https://definitions.baldir.fr',
      ],
      numberOfRuns: 3, // Set low to speed up the test runs. Default is 3.
      headful: false, // Show the browser which is helpful when checking the config
      settings: {
        disableStorageReset: true, // Don't clear localStorage / IndexedDB / etc before loading the page
        preset: 'desktop',
        chromeFlags: '--headless --disable-gpu --no-sandbox --disable-dev-shm-usage --no-first-run'
      }
    },
    upload: {
      target: 'filesystem',
      outputDir:"tmp/lighthouse",
      reportFilenamePattern: '%%HOSTNAME%%-%%PATHNAME%%-%%DATE%%.%%EXTENSION%%',
    },
  }
};
